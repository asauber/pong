import pygame
BLACK = (0, 0, 0)

class Paddle(pygame.sprite.Sprite):

    def __init__(self, image, color, width, height):
        # Call the parent class (Sprite) constructor
        super().__init__()

        print("loading image", image)
        self.image = pygame.image.load(image).convert_alpha()
        self.image = pygame.transform.scale(self.image, (width, height))

        self.rect = self.image.get_rect()

    def move_up(self, pixels):
        self.rect.y -= pixels
        if self.rect.y < 0:
            self.rect.y = 0

    def move_down(self, pixels):
        self.rect.y += pixels
        if self.rect.y > 400:
            self.rect.y = 400

