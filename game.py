import pygame

from paddle import Paddle
from ball import Ball

pygame.init()

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

size = (700, 500)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Pong")
width = 30

paddle_a = Paddle("./doritos.jpg", WHITE, width,  100)
paddle_a.rect.x = width
paddle_a.rect.y = 200

paddle_b = Paddle("./lays.jpg", WHITE, width, 100)
paddle_b.rect.x = 670
paddle_b.rect.y = 200

ball = Ball(WHITE, 10, 10)
ball.rect.x = 345
ball.rect.y = 195

all_sprites_list = pygame.sprite.Group()

all_sprites_list.add(paddle_a)
all_sprites_list.add(paddle_b)
all_sprites_list.add(ball)

clock = pygame.time.Clock()

score_a = 0
score_b = 0

run_game = True

while run_game:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run_game = False
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_x:
            run_game = False

    keys = pygame.key.get_pressed()

    speed = 10

    if keys[pygame.K_w]:
        paddle_a.move_up(speed)
    if keys[pygame.K_s]:
        paddle_a.move_down(speed)

    if keys[pygame.K_UP]:
        paddle_b.move_up(speed)
    if keys[pygame.K_DOWN]:
        paddle_b.move_down(speed)

    all_sprites_list.update()

    if ball.rect.x >= 690:
        score_a += 1
        ball.velocity[0] = -ball.velocity[0]
    if ball.rect.x <= 0:
        score_b += 1
        ball.velocity[0] = -ball.velocity[0]
    if ball.rect.y > 490:
        ball.velocity[1] = -ball.velocity[1]
    if ball.rect.y < 0:
        ball.velocity[1] = -ball.velocity[1]

    if pygame.sprite.collide_mask(ball, paddle_a) or pygame.sprite.collide_mask(ball, paddle_b):
        ball.bounce()

    screen.fill(BLACK)
    pygame.draw.line(screen, WHITE, [349, 0], [349, 500], 5)

    all_sprites_list.draw(screen)

    font = pygame.font.Font(None, 74)
    text = font.render(str(score_a), 1, WHITE)
    screen.blit(text, (250, 10))
    text = font.render(str(score_b), 1, WHITE)
    screen.blit(text, (420, 10))

    pygame.display.flip()

    winning_score = 25
    if score_a == winning_score or score_b == winning_score:
        score_a = 0
        score_b = 0
        ball.rect.x = 345
        ball.rect.y = 195
        pygame.time.delay(3000)

    clock.tick(60)

pygame.quit()
